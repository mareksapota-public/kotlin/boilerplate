# Copyright 2020, 2021 Marek Sapota

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

.PHONY: build rebuild gradle-config gitlab-config init-boilerplate

KOTLIN_BOILERPLATE_CWD := $(dir $(realpath $(lastword $(MAKEFILE_LIST))))
KOTLIN_BOILERPLATE_PWD := $(shell pwd)

KOTLIN_BOILERPLATE_BUILD_DEPS := build.config.json
KOTLIN_BOILERPLATE_BUILD_DEPS += ${KOTLIN_BOILERPLATE_CWD}/../gradle-build/templates/build.gradle.kts.template
KOTLIN_BOILERPLATE_BUILD_DEPS += ${KOTLIN_BOILERPLATE_CWD}/../gradle-build/templates/jar.gradle.kts.template
KOTLIN_BOILERPLATE_BUILD_DEPS += ${KOTLIN_BOILERPLATE_CWD}/../gradle-build/templates/settings.gradle.kts.template
KOTLIN_BOILERPLATE_BUILD_DEPS += ${KOTLIN_BOILERPLATE_CWD}/../gradle-build/generate-gradle-build.py

GITLAB_CONFIGS := $(wildcard .gitlab-ci.d/*.yml)

build: init-boilerplate
	gradle --console=plain build

rebuild: init-boilerplate
	gradle --console=plain build --rerun-tasks

gradle-config: build.gradle.kts settings.gradle.kts

build.gradle.kts settings.gradle.kts &: $(KOTLIN_BOILERPLATE_BUILD_DEPS)
	"${KOTLIN_BOILERPLATE_CWD}/../gradle-build/generate-gradle-build.py" \
		"${KOTLIN_BOILERPLATE_PWD}/build.config.json" \
		"${KOTLIN_BOILERPLATE_PWD}/build.gradle.kts" \
		"${KOTLIN_BOILERPLATE_PWD}/settings.gradle.kts"

gitlab-config: .gitlab-ci.yml

.gitlab-ci.yml: ${KOTLIN_BOILERPLATE_CWD}/../gitlab/gitlab-ci.yml .gitlab-ci.d $(GITLAB_CONFIGS)
	cp "${KOTLIN_BOILERPLATE_CWD}/../gitlab/gitlab-ci.yml" "${KOTLIN_BOILERPLATE_PWD}/.gitlab-ci.yml"
ifneq ($(GITLAB_CONFIGS),)
	echo $(GITLAB_CONFIGS) | sort | xargs cat >> .gitlab-ci.yml
endif

# Local customization files
.gitlab-ci.d:
	mkdir .gitlab-ci.d

init-boilerplate: .gitignore .gitlab gradlew gradle-config gitlab-config

.gitignore:
	cp "${KOTLIN_BOILERPLATE_CWD}/../.gitignore" .gitignore

.gitlab:
	ln -s "$(shell realpath "--relative-to=${KOTLIN_BOILERPLATE_PWD}" "${KOTLIN_BOILERPLATE_CWD}/../gitlab/")" .gitlab

gradlew:
	ln -s "$(shell realpath "--relative-to=${KOTLIN_BOILERPLATE_PWD}" "${KOTLIN_BOILERPLATE_CWD}/../gradle/gradlew")" gradlew
