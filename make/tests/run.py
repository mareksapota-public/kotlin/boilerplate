#!/usr/bin/env python3
# Copyright 2020, 2021 Marek Sapota

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import functools
import os
import pathlib
import shutil
import subprocess
import tempfile
import unittest


class TestMakefile(unittest.TestCase):
    @property
    @functools.lru_cache
    def cwd(self):
        return str(pathlib.Path(__file__).parent.absolute())

    def init_makefile(self, tmpdir):
        with open(f'{tmpdir}/Makefile', 'w') as f:
            f.write(f'include {self.cwd}/../Makefile.mk\n')
        shutil.copy(
            pathlib.Path(f'{self.cwd}/build.config.json'),
            pathlib.Path(f'{tmpdir}/build.config.json'),
        )

    def test_makefile(self):
        with tempfile.TemporaryDirectory() as tmpdir:
            self.init_makefile(tmpdir)
            subprocess.run(
                ['make', 'init-boilerplate'],
                cwd=tmpdir,
                check=True,
            )
            for filename in [
                '.gitignore',
                '.gitlab-ci.yml',
                '.gitlab-ci.d',
                'build.gradle.kts',
                'settings.gradle.kts',
                'gradlew',
            ]:
                self.assertTrue(pathlib.Path(f'{tmpdir}/{filename}').exists())
            self.assertTrue(pathlib.Path(f'{tmpdir}/.gitlab').is_symlink())

    def test_gitlab_ci(self):
        with tempfile.TemporaryDirectory() as tmpdir:
            self.init_makefile(tmpdir)
            os.mkdir(f'{tmpdir}/.gitlab-ci.d')
            gci1 = pathlib.Path(f'{tmpdir}/.gitlab-ci.d/gitlab-ci1.yml')
            gci2 = pathlib.Path(f'{tmpdir}/.gitlab-ci.d/gitlab-ci2.yml')

            shutil.copy(pathlib.Path(f'{self.cwd}/gitlab-ci1.yml'), gci1)
            subprocess.run(
                ['make', 'init-boilerplate'],
                cwd=tmpdir,
                check=True,
            )
            with open(f'{tmpdir}/.gitlab-ci.yml') as f:
                data = f.read()
                self.assertTrue('foo:' in data and '- bar' in data)

            shutil.copy(pathlib.Path(f'{self.cwd}/gitlab-ci2.yml'), gci2)
            # Make ignores changes if the difference is too small.
            while (
                gci2.stat().st_mtime - gci1.stat().st_mtime < 0.5
            ):
                gci2.touch()
            subprocess.run(
                ['make', 'init-boilerplate'],
                cwd=tmpdir,
                check=True,
            )
            with open(f'{tmpdir}/.gitlab-ci.yml') as f:
                data = f.read()
                self.assertTrue('baz:' in data and 'foo:' in data)


if __name__ == '__main__':
    unittest.main()
