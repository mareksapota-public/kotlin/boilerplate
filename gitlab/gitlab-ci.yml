# Copyright 2020, 2021 Marek Sapota

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

include:
  - template: Dependency-Scanning.gitlab-ci.yml
  - template: Security/License-Scanning.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml

.base_job_template: &base_job
  before_script:
    - /usr/bin/gradle --version

.kotlin_job_cache_template: &kotlin_job_cache
  paths:
      - .gradle/
      - build/
      - vendor/*/.gradle/
      - vendor/*/build/

.kotlin_job_template: &kotlin_job_15
  <<: *base_job
  image: "gradle:jdk15"
  cache:
    key: ${CI_COMMIT_REF_SLUG}_java15
    <<: *kotlin_job_cache

.kotlin_job_template_14: &kotlin_job_14
  <<: *base_job
  image: "gradle:jdk14"
  allow_failure: true
  cache:
    key: ${CI_COMMIT_REF_SLUG}_java14
    <<: *kotlin_job_cache

.build_job_template: &build_job
  stage: build

  script:
    - /usr/bin/gradle assemble --no-daemon --console=plain

.test_job_template: &test_job
  stage: test

  script:
    - /usr/bin/gradle check --no-daemon --warning-mode=all --console=plain
    - ./.gitlab/print-test-coverage.sh

.release_job_template: &release_job
  stage: release

  script:
    # - /usr/bin/gradle proguard --no-daemon --console=plain
    - pushd build/libs/
    - rm -f checksum.sha512
    - sha512sum * > checksum.sha512
    - popd

  artifacts:
    paths:
      - build/reports/
      - build/libs/
    reports:
      junit: build/test-results/test/TEST-*.xml

variables:
  GRADLE_USER_HOME: "${CI_PROJECT_DIR}/.gradle/"
  GIT_SUBMODULE_STRATEGY: recursive

stages:
  - build
  - test
  - release

build_kotlin:
  <<: *kotlin_job_14
  <<: *build_job

test_kotlin:
  needs:
    - job: build_kotlin
      artifacts: true
  <<: *kotlin_job_14
  <<: *test_job

gemnasium-dependency_scanning:
  dependencies: ["build_kotlin"]

release_kotlin:
  needs:
    - job: test_kotlin
      artifacts: true
  <<: *kotlin_job_14
  <<: *release_job
