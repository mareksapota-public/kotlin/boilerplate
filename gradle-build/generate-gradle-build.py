#!/usr/bin/env python3
# Copyright 2020 Marek Sapota

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import argparse
import json
import pathlib
import sys
import textwrap
from distutils.version import StrictVersion
from typing import List, Tuple

if sys.version_info.minor >= 8:
    from typing import TypedDict
else:
    from mypy_extensions import TypedDict

AUTO_GENERATED_HEADER = (
    "// This file has been auto generated, do not edit directly"
)

parser = argparse.ArgumentParser()
parser.add_argument('config_file', help='Configuration file to use')
parser.add_argument('build_file', help='Build file to generate')
parser.add_argument('settings_file', help='Settings file to generate')
args = parser.parse_args()


class ProjectDependency(TypedDict):
    group: str
    project: str
    location: str


class JarConfig(TypedDict):
    name: str
    main_class: str


class RequiredConfiguration(TypedDict):
    name: str
    group: str


class Configuration(RequiredConfiguration, total=False):
    dependencies: List[Tuple[str, str]]
    project_dependencies: List[ProjectDependency]
    default_jar: bool
    jars: List[JarConfig]


def jar_job(name):
    return name.replace('-', '_').replace('.', '_') + "_jar"


def read_file(path: str, *, remove_comments: bool = False) -> str:
    with open(path, 'r') as f:
        contents = f.read()
    if remove_comments:
        contents = '\n'.join([
            line for line in contents.splitlines()
            if not (line.startswith('//') or line == '')
        ])
    return contents


cwd = str(pathlib.Path(__file__).parent.absolute())
configuration = Configuration(json.loads(read_file(args.config_file)))
jar_template = read_file(
    cwd + '/templates/jar.gradle.kts.template',
    remove_comments=True,
)
build_template = read_file(cwd + '/templates/build.gradle.kts.template')
settings_template = read_file(cwd + '/templates/settings.gradle.kts.template')

all_dependencies = []
all_project_dependencies = []
all_included_builds = []
all_jar_templates = []
all_jar_assembles = []
all_proguard_jars = []

for dep_config in configuration.get('dependencies', []):
    all_dependencies.append(
        f'{dep_config[0]}("{dep_config[1]}")',
    )

for pdep_config in configuration.get('project_dependencies', []):
    project = pdep_config['project']
    group = pdep_config['group']
    location = pdep_config['location']
    all_dependencies.append(
        f'implementation("{group}:{project}:current")',
    )
    all_project_dependencies.append(
        'compileKotlin.dependsOn('
        f'gradle.includedBuild("{project}").task(":assemble")'
        ')',
    )
    all_included_builds.append(
        f'includeBuild("{location}")',
    )

for jar_config in configuration.get('jars', []):
    template = jar_template \
        .replace('TEMPLATE_MAIN_CLASS', jar_config['main_class'])
    jar_name = jar_job(jar_config['name'])
    archive_name = jar_config['name']
    all_jar_templates.append(
        template
            .replace('TEMPLATE_JAR_NAME', jar_name)
            .replace('TEMPLATE_ARCHIVE_NAME', archive_name)
    )
    all_jar_assembles.append(f'dependsOn("{jar_name}")')
    all_proguard_jars.append(f'dependsOn("{jar_name}_proguard")')

default_jar = len(configuration.get('jars', [])) == 0
if 'default_jar' in configuration:
    default_jar = configuration['default_jar']

template_include_build = '\n'.join(all_included_builds)
if len(all_included_builds) > 0:
    template_include_build = f'\n{template_include_build}\n'

for file_path in (args.build_file, args.settings_file, ):
    if pathlib.Path(file_path).exists():
        output_contents = read_file(file_path)
        if AUTO_GENERATED_HEADER not in output_contents:
            raise RuntimeError('Not overwriting existing output file')

with open(args.build_file, 'w') as f:
    f.write(AUTO_GENERATED_HEADER)
    f.write('\n')
    template_jars = textwrap.indent('\n'.join(all_jar_templates), ' ' * 4)
    if len(all_jar_templates) > 0:
        template_jars += '\n'
    template_assemble_jars = \
        textwrap.indent('\n'.join(all_jar_assembles), ' ' * 8)
    if len(all_jar_assembles) > 0:
        template_assemble_jars += '\n'
    template_proguard_jars = \
        textwrap.indent('\n'.join(all_proguard_jars), ' ' * 8)
    if len(all_proguard_jars) > 0:
        template_proguard_jars += '\n'
    f.write(
        build_template
            .replace(
                'TEMPLATE_DEPENDENCIES\n',
                textwrap.indent('\n'.join(all_dependencies), ' ' * 4) +
                    '\n' if len(all_dependencies) > 0 else '',
            )
            .replace('TEMPLATE_JARS\n', template_jars)
            .replace(
                'TEMPLATE_ASSEMBLE_JARS\n',
                template_assemble_jars,
            )
            .replace(
                'TEMPLATE_PROGUARD_JARS\n',
                template_proguard_jars,
            )
            .replace(
                'TEMPLATE_PROGUARD_IMPORT\n',
                'import proguard.gradle.ProGuardTask\n'
                    if len(all_proguard_jars) > 0 else '',
            )
            .replace(
                'TEMPLATE_PROJECT_DEPENDENCIES\n',
                '\n'.join(
                    (
                        ['\nval compileKotlin: KotlinCompile by tasks']
                            if len(all_project_dependencies) > 0 else []
                    ) +
                    all_project_dependencies,
                ),
            ).replace('TEMPLATE_GROUP', configuration['group'])
            .replace('TEMPLATE_DEFAULT_JAR', 'true' if default_jar else 'false')
    )

with open(args.settings_file, 'w') as f:
    f.write(AUTO_GENERATED_HEADER)
    f.write('\n')
    f.write(
        settings_template
            .replace('TEMPLATE_NAME', configuration['name'])
            .replace('\nTEMPLATE_INCLUDE_BUILD\n', template_include_build)
    )
