// This file has been auto generated, do not edit directly
// Copyright 2020 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

group = "mygroup3"

repositories {
    jcenter()
    mavenCentral()
}

buildscript {
    repositories {
        google()
        jcenter()
    }
    dependencies {
        classpath("com.guardsquare:proguard-gradle:7.0.1")
    }
}

plugins {
    kotlin("jvm") version "1.4.31"
    id("org.jlleitschuh.gradle.ktlint") version "10.0.0"
    // Test coverage
    id("jacoco")
    // Gradle check for version updates
    id("com.github.ben-manes.versions") version "0.36.0"
}

ktlint {
    version.set("0.40.0")
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.4.31")
}

sourceSets {
    main {
        java.srcDir("src/main/kotlin/")
    }
}

val dep_classes = configurations.runtimeClasspath.get().map {
    if (it.isDirectory) it else zipTree(it)
}

val jar: Jar by tasks
jar.enabled = true

tasks {
    withType<Jar> {
        duplicatesStrategy = DuplicatesStrategy.EXCLUDE
    }

    withType<KotlinCompile>().configureEach {
        kotlinOptions.jvmTarget = JavaVersion.VERSION_1_8.toString()
        kotlinOptions.freeCompilerArgs += "-Xopt-in=kotlin.RequiresOptIn"
        kotlinOptions.freeCompilerArgs += "-Xopt-in=kotlin.time.ExperimentalTime"
    }

    val proguard by creating(DefaultTask::class) {
    }

    withType<Test> {
        useJUnitPlatform()
        finalizedBy("jacocoTestReport")
    }

    withType<DependencyUpdatesTask> {
        checkForGradleUpdate = false
        outputDir = "build/reports/dependencyUpdates"
    }

    named("dependencyUpdates", DependencyUpdatesTask::class.java).configure {
        // Ignore non-stable versions.
        rejectVersionIf {
            candidate.version.contains("-RC") ||
                ".*-M[0-9]".toRegex().matches(candidate.version) ||
                ".*-beta[0-9]".toRegex().matches(candidate.version)
        }
    }

    assemble {
    }
}

tasks.jacocoTestReport {
    reports {
        xml.isEnabled = true
        csv.isEnabled = false
        html.isEnabled = true
        html.destination = file("$buildDir/reports/jacoco/")
    }
}
