#!/usr/bin/env python3
# Copyright 2020, 2021 Marek Sapota

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import difflib
import pathlib
import subprocess
import tempfile
import unittest


class TestGradleBuild(unittest.TestCase):
    maxDiff = None

    def check_correct(self, correct_file: str, actual_file: str) -> None:
        with open(correct_file) as f:
            correct = f.read()
        with open(actual_file) as f:
            actual = f.read()
        self.assertEqual(
            correct,
            actual,
            '\n'.join(difflib.unified_diff(
                correct.splitlines(),
                actual.splitlines(),
                fromfile='correct',
                tofile='actual',
            )),
        )

    def run_test(self, config_name: str, build_name: str, settings_name: str):
        cwd = str(pathlib.Path(__file__).parent.absolute())
        with tempfile.TemporaryDirectory() as tmpdir:
            cmd = [
                f'{cwd}/../generate-gradle-build.py',
                f'{cwd}/{config_name}',
                f'{tmpdir}/build',
                f'{tmpdir}/settings',
            ]
            subprocess.run(cmd, check=True)
            self.check_correct(f"{cwd}/{build_name}", f"{tmpdir}/build")
            self.check_correct(f"{cwd}/{settings_name}", f"{tmpdir}/settings")


    def test_1(self):
        self.run_test('config1.json', 'build1.kts', 'settings1.kts')

    def test_2(self):
        self.run_test('config2.json', 'build2.kts', 'settings2.kts')

    def test_3(self):
        self.run_test('config3.json', 'build3.kts', 'settings3.kts')

if __name__ == '__main__':
    unittest.main()
