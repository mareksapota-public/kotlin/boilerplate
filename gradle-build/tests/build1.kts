// This file has been auto generated, do not edit directly
// Copyright 2020 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import proguard.gradle.ProGuardTask

group = "mygroup"

repositories {
    jcenter()
    mavenCentral()
}

buildscript {
    repositories {
        google()
        jcenter()
    }
    dependencies {
        classpath("com.guardsquare:proguard-gradle:7.0.1")
    }
}

plugins {
    kotlin("jvm") version "1.4.31"
    id("org.jlleitschuh.gradle.ktlint") version "10.0.0"
    // Test coverage
    id("jacoco")
    // Gradle check for version updates
    id("com.github.ben-manes.versions") version "0.36.0"
}

ktlint {
    version.set("0.40.0")
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.4.31")
    implementation("com.foo:foo:1.0.0")
    testImplementation("com.bar:bar:2.0.0")
    implementation("othergroup:otherproject:current")
}

val compileKotlin: KotlinCompile by tasks
compileKotlin.dependsOn(gradle.includedBuild("otherproject").task(":assemble"))
sourceSets {
    main {
        java.srcDir("src/main/kotlin/")
    }
}

val dep_classes = configurations.runtimeClasspath.get().map {
    if (it.isDirectory) it else zipTree(it)
}

val jar: Jar by tasks
jar.enabled = false

tasks {
    val jar1_jar by creating(Jar::class) {
        archiveFileName.value("jar1.jar")
        manifest {
            attributes["Main-Class"] = "com.baz.main1"
        }
        // Include class files.
        from(sourceSets.getByName("main").output)
        // Include classes from dependencies.
        from(dep_classes)
    }
    val jar1_jar_proguard by creating(ProGuardTask::class) {
        dependsOn(jar1_jar)
        verbose()
        dontobfuscate()
        injars("build/libs/jar1.jar")
        outjars("build/libs/jar1.stripped.jar")
        // Define standard library
        listOf(
            "base",
            "desktop",
            "logging",
            "management",
            "scripting",
            "sql",
            "xml"
        ).forEach {
            libraryjars(
                // Duplicate class errors without this
                mapOf(
                    "jarfilter" to "!**.jar",
                    "filter" to "!module-info.class"
                ),
                "${System.getProperty("java.home")}/jmods/java.$it.jmod"
            )
        }
        // Specify class and method that is the entry point and should not be stripped
        keep(
            """class com.baz.main1 {
                public static void main(java.lang.String[]);
            }
            """
        )
        // Should be fixed in future versions
        // https://github.com/Kotlin/kotlinx.coroutines/issues/2046
        dontwarn("java.lang.instrument.ClassFileTransformer")
        dontwarn("sun.misc.SignalHandler")
        dontwarn("java.lang.instrument.Instrumentation")
        dontwarn("sun.misc.Signal")
        dontwarn("kotlin.time.Duration\$Companion")
        // Not mentioned in the issue, probably/hopefully also safe to ignore
        dontwarn("android.annotation.SuppressLint")
    }
    jar1_jar_proguard.enabled = JavaVersion.current() != JavaVersion.VERSION_15
    val jar2_jar by creating(Jar::class) {
        archiveFileName.value("jar2.jar")
        manifest {
            attributes["Main-Class"] = "com.baz.main2"
        }
        // Include class files.
        from(sourceSets.getByName("main").output)
        // Include classes from dependencies.
        from(dep_classes)
    }
    val jar2_jar_proguard by creating(ProGuardTask::class) {
        dependsOn(jar2_jar)
        verbose()
        dontobfuscate()
        injars("build/libs/jar2.jar")
        outjars("build/libs/jar2.stripped.jar")
        // Define standard library
        listOf(
            "base",
            "desktop",
            "logging",
            "management",
            "scripting",
            "sql",
            "xml"
        ).forEach {
            libraryjars(
                // Duplicate class errors without this
                mapOf(
                    "jarfilter" to "!**.jar",
                    "filter" to "!module-info.class"
                ),
                "${System.getProperty("java.home")}/jmods/java.$it.jmod"
            )
        }
        // Specify class and method that is the entry point and should not be stripped
        keep(
            """class com.baz.main2 {
                public static void main(java.lang.String[]);
            }
            """
        )
        // Should be fixed in future versions
        // https://github.com/Kotlin/kotlinx.coroutines/issues/2046
        dontwarn("java.lang.instrument.ClassFileTransformer")
        dontwarn("sun.misc.SignalHandler")
        dontwarn("java.lang.instrument.Instrumentation")
        dontwarn("sun.misc.Signal")
        dontwarn("kotlin.time.Duration\$Companion")
        // Not mentioned in the issue, probably/hopefully also safe to ignore
        dontwarn("android.annotation.SuppressLint")
    }
    jar2_jar_proguard.enabled = JavaVersion.current() != JavaVersion.VERSION_15
    withType<Jar> {
        duplicatesStrategy = DuplicatesStrategy.EXCLUDE
    }

    withType<KotlinCompile>().configureEach {
        kotlinOptions.jvmTarget = JavaVersion.VERSION_1_8.toString()
        kotlinOptions.freeCompilerArgs += "-Xopt-in=kotlin.RequiresOptIn"
        kotlinOptions.freeCompilerArgs += "-Xopt-in=kotlin.time.ExperimentalTime"
    }

    val proguard by creating(DefaultTask::class) {
        dependsOn("jar1_jar_proguard")
        dependsOn("jar2_jar_proguard")
    }

    withType<Test> {
        useJUnitPlatform()
        finalizedBy("jacocoTestReport")
    }

    withType<DependencyUpdatesTask> {
        checkForGradleUpdate = false
        outputDir = "build/reports/dependencyUpdates"
    }

    named("dependencyUpdates", DependencyUpdatesTask::class.java).configure {
        // Ignore non-stable versions.
        rejectVersionIf {
            candidate.version.contains("-RC") ||
                ".*-M[0-9]".toRegex().matches(candidate.version) ||
                ".*-beta[0-9]".toRegex().matches(candidate.version)
        }
    }

    assemble {
        dependsOn("jar1_jar")
        dependsOn("jar2_jar")
    }
}

tasks.jacocoTestReport {
    reports {
        xml.isEnabled = true
        csv.isEnabled = false
        html.isEnabled = true
        html.destination = file("$buildDir/reports/jacoco/")
    }
}
